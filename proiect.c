#include<stdio.h>
#include<stdlib.h>

//Structura pentru nodul arborelui binar
struct Node
{
    int nr;
    struct Node *stanga;
    struct Node *dreapta;
};

//Functie pentru a crea un nou nod
struct Node *creare(int nr)
{
    struct Node *nou=(struct Node*)malloc(sizeof(struct Node));
    nou->nr=nr;
    nou->stanga=NULL;
    nou->dreapta=NULL;
    return nou;
}

//Functie pentru a insera un element in arborele binar ordonat
struct Node *inserare(struct Node *rad,int nr)
{
    if(rad==NULL)
    {
        return creare(nr);
    }
    if(nr<rad->nr)
    {
        rad->stanga=inserare(rad->stanga,nr);
    }
    else if(nr>rad->nr)
    {
        rad->dreapta=inserare(rad->dreapta,nr);
    }
    return rad;
}

struct Node *generateRandomOrderedTree(int n)
{
    int i;
    int *randomNumbers=(int*)malloc(n*sizeof(int));
    for(i=0;i<n;i++)
    {
        randomNumbers[i]=rand()%100; //generati numere aleatoare intre 0 si 99
    }

    struct Node *rad=NULL;
    for(i=0;i<n;i++)
    {
        rad=inserare(rad,randomNumbers[i]);
    }

    free(randomNumbers); //Elibereaza memoria alocate pentru sirul de numere aleatoare
    
    return rad;
}

//Functie pentru traversarea in preordine
void Preordine(struct Node *rad)
{
    if(rad!=NULL)
    {
        printf("%d ",rad->nr);
        Preordine(rad->stanga);
        Preordine(rad->dreapta);
    }
}

//Functie pentru traversarea in inordine
void Inordine(struct Node *rad)
{
    if(rad!=NULL)
    {
        Inordine(rad->stanga);
        printf("%d ",rad->nr);
        Inordine(rad->dreapta);
    }
}

//Functie pentru traversarea in postordine
void Postordine(struct Node *rad)
{
    if(rad!=NULL)
    {
        Postordine(rad->stanga);
        Postordine(rad->dreapta);
        printf("%d ",rad->nr);
    }
}

void printGivenLevel(struct Node*rad,int level)
{
    if(rad==NULL)
    {
        return;
    }
    if(level==1)
    {
        printf("%d ",rad->nr);
    }
    else if(level>1)
    {
        printGivenLevel(rad->stanga,level-1);
        printGivenLevel(rad->dreapta,level-1);
    }
}

int getHeight(struct Node *rad)
{
    if(rad==NULL)
    {
        return 0;
    }
    else
    {
        int leftHeight=getHeight(rad->stanga);
        int rightHeight=getHeight(rad->dreapta);
        return (leftHeight>rightHeight)?(leftHeight+1):(rightHeight+1);
    }
}

void levelOrdelTraversal(struct Node *rad)
{
    int h=getHeight(rad);
    for(int i=1;i<=h;i++)
    {
        printGivenLevel(rad,i);
    }
}

int main()
{
    //Generati un arbore binar cu elemente aleatoare si ordonate
    struct Node *rad=generateRandomOrderedTree(8);

    printf("Preordine: ");
    Preordine(rad);
    printf("\n");

    printf("Inordine: ");
    Inordine(rad);
    printf("\n");

    printf("Postordine: ");
    Postordine(rad);
    printf("\n");

    printf("Level-order: ");
    levelOrdelTraversal(rad);
    printf("\n");

    return 0;
}