# proiect-sincretic-Mihaila-Ioana

## Nume
TRAVERSAREA ARBORILOR BINARI ORDONATI

## Descriere
Acest program in limbajul C implementeaza pe un arbore binar urmatoarele operatii: inserarea de elemente aleatorii intr-un arbore binar ordonat, traversarea arborelui in preordine, inordine, postordine si level-order.

### Structura de cod

În codul de mai sus, se definește o structură pentru nodul unui arbore binar și funcțiile asociate:

- `creare`: Funcția pentru crearea unui nou nod în arbore.
- `inserare`: Funcția pentru inserarea unui element într-un arbore binar ordonat.
- `generateRandomOrderedTree`: Funcția pentru generarea unui arbore binar ordonat cu `n` elemente aleatorii.
- Funcțiile de traversare:
  - `Preordine`: Traversarea în preordine a arborelui.
  - `Inordine`: Traversarea în inordine a arborelui.
  - `Postordine`: Traversarea în postordine a arborelui.
  - `printGivenLevel` și `getHeight`: Funcții auxiliare pentru traversarea în level-order a arborelui.
- `levelOrdelTraversal`: Funcția principală pentru traversarea în level-order a arborelui.

### Cum se utilizează codul

În funcția `main()`, se generează un arbore binar cu elemente aleatoare și ordonate. Apoi, sunt afișate rezultatele traversărilor în diferite ordini: preordine, inordine, postordine și level-order.

Pentru a utiliza acest cod sau a extinde funcționalitățile, puteți modifica parametrii sau adăuga alte operații pe arborele binar în funcția `main()` sau în alte funcții definite.

## Instalare
Acest program este scris in limbajul C si poate fi compilat folosind un compilator precum GCC. Urmeaza acesti pasi pentru a compila si executa programul:

 **Descarca codul sursa:** Descarca sau cloneaza acest repository pe computerul tau local.


Pentru a executa acest program, asigura-te ca ai un compilator C instalat in prealabil, cum ar fi GCC. Poti instala GCC si alte unelte de dezvoltare C prin intermediul managerului de pachete al sistemului tau.

De asemenea, prin imaginea publica care este disponibila pe Docker Hub, poti rula programul pe orice masina prin descarcarea imaginii, folosind comanda 'docker pull ioanamihaila300403/proiect-sincretic:traversari-arbore-binar'


## Utilizare
1. **Deschide terminalul:** Navigheaza in directorul unde ai descarcat codul sursa folosind terminalul sau linia de comanda.

2. **Compileaza programul:** Foloseste un compilator C, cum ar fi GCC, pentru a compila codul sursa. Poti folosi comanda:
    ```
    gcc nume_program.c -o nume_executabil
    ```
    Asigura-te ca inlocuiesti `nume_program.c` cu numele fisierului care contine codul sursa si `nume_executabil` cu numele dorit pentru executabilul rezultat.

3. **Executa programul:** Dupa ce ai compilat cu succes, ruleaza programul generat:
    ```
    ./nume_executabil
    ```
    Aici, `nume_executabil` este numele pe care l-ai dat executabilului in pasul anterior.

4. **Urmeaza instructiunile din program:** Programul va afisa traversarile arborelui generat automat cu elemente aleatorii in preordine, inordine, postordine si level-order.

5. **Observa rezultatele:** Programul va afisa elementele arborelui binar ordonat in ordinea specificata.

Totodata, prin descarcarea imaginii publice de pe Docker Hub, nu va mai trebui sa ai descarcat un compilator GCC pe masina ta, in Dockerfile preluandu-se imaginea GCC (a compilatorului pentru limbajele de programare C si C++).

## Docker Hub

Am utilizat mediul Docker Desktop pentru a realiza o imagine prin care sa pot rula in terminal codul sursa. Am creat un Dockerfile unde am specificat: imaginea de baza pe care am utilizat-o pentru a construi imaginea mea, adica imaginea compilatorului GCC din Docker Hub, copierea fisierului sursa 'proiect.c' din directorul local in directorul /appC/ din containerul Docker, setarea directorului curent in interiorul containerului /WORKDIR /appC', comanda pentru rulare in container si pentru compilarea fisierului sursa, creand si un executabil si comanda care va fi executata cand containerul este pornit si va rula executabilul generat anterior.

Apoi am incarcat imaginea intr-un repository public pe Docker Hub, de unde poate fi preluata si rulata de cadrul didactic.

## Suport
Pentru orice intrebari sau suport, te rugam sa contactezi pe Mihaila Ioana

## Contributii
Contributiile sunt binevenite.

## Autor
Mihaila Ioana

## Statusul proiectului
Acest proiect este functional si toate functionalitatiile sunt implementate(generarea arborelui binar ordonat cu elemente aleatorii, implementarea functiilor de traversare in inordine, preordine, postordine si pe nivel). Totodata, se planifica crearea si publicarea unei imagini a programului pe Docker Hub pentru a usura procesul de distribuire si utilizare.

| Task                                           | Stare   |
| ---------------------------------------------- | ------- |
| Generarea arborelui binar ordonat              | DONE    |
| Adăugarea elementelor aleatorii                | DONE    |
| Implementarea funcției de traversare în preordine | DONE  |
| Implementarea funcției de traversare în inordine | DONE   |
| Implementarea funcției de traversare în postordine| DONE   |
| Implementarea funcției de traversare în level-order | DONE |
| Crearea și publicarea imaginii programului pe Docker Hub pentru a facilita distribuirea și utilizarea acestuia | DONE |