
FROM gcc:latest
COPY proiect.c /appC/
WORKDIR /appC
RUN gcc -o proiect proiect.c -lstdc++
CMD ["./proiect"]